import { Router } from 'express';
import { checkJwt } from '../middlewares/checkJWT';
import ShopRoute from './shop.route';
// import auth from "./auth";
import UserRoute from './user.route';

const routes = Router();

routes.use('/user', UserRoute);
routes.use('/shop', [checkJwt], ShopRoute);

export default routes;

import { Router } from 'express';
import UserController from '../controllers/UserController';

const router = Router();

//Create a new user
router.post('/', UserController.newUser);
router.post('/login', UserController.login);
router.post('/refresh', UserController.refresh);

export default router;

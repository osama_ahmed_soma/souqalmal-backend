import { Router } from 'express';
import ShopController from '../controllers/ShopController';

const router = Router();

//Create a new user
router.get('/nearBy', ShopController.listNearBy);
router.get('/preferred', ShopController.listPreferred);
router.post('/like', ShopController.like);
router.post('/disLike', ShopController.disLike);
router.post('/remove', ShopController.remove);
router.post('/', ShopController.newShop);

export default router;

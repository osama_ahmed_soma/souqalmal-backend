import BodyParser from 'body-parser';
import cors from 'cors';
import Express from 'express';
import helmet from 'helmet';
import * as path from 'path';
import 'reflect-metadata';
import {
  ConnectionOptions,
  createConnection,
  getConnectionOptions
} from 'typeorm';
import routes from './routes';

const main = async () => {
  const connectionOptions: ConnectionOptions = await getConnectionOptions(
    process.env.NODE_ENV === 'production' ? 'production' : 'default'
  );
  await createConnection({
    ...connectionOptions,
    name: 'default'
  });
  const app = Express();
  app.use(Express.static('assets'));
  app.use(cors());
  app.use(helmet());
  app.use(BodyParser.json());

  app.use('/api', routes);

  const port = process.env.PORT || 8080;
  app.use(Express.static(path.join(__dirname, 'assets')));
  app.listen(port, () => {
    console.log(`server is running on post ${port} ${process.env.SERVER_URL}`);
  });
};
main();

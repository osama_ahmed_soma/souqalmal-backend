import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Shop } from '../entity/Shop';
import { UserShop } from '../entity/UserShop';
import JWTPayloadInterface from '../interfaces/JWTPayloadInterface';
import ShopInputInterface from '../interfaces/ShopInputInterface';
import ShopLikeInputInterface from '../interfaces/ShopLikeInputInterface';
import UserController from './UserController';

class ShopController {
  static _getAll = async () => {
    const shopRepository = getRepository(Shop);
    return shopRepository.find();
  };

  static getOneById = async (id: number) => {
    const shopRepository = getRepository(Shop);
    try {
      return shopRepository.findOneOrFail(id, {
        select: ['id', 'name', 'picture', 'coordinates']
      });
    } catch (error) {
      return undefined;
    }
  };

  static listNearBy = async (_: Request, res: Response) => {
    const shopRepository = getRepository(Shop);
    const {
      coordinates: { lat, lng }
    }: JWTPayloadInterface = res.locals.jwtPayload;
    // const now = new Date();
    const query = shopRepository
      .createQueryBuilder('shop')
      .leftJoinAndSelect('shop.userShops', 'userShops')
      .where(
        `ST_DWithin(shop.coordinates, ST_GeomFromGeoJSON(:origin), 0.14) AND (userShops.isLike = :isLike OR userShops.isLike IS NULL) AND (userShops.lastRemoveDate IS NULL OR userShops.lastRemoveDate + '2 hours' < now())`
      ) // 14 km
      .orderBy({
        'ST_DWithin(shop.coordinates, ST_GeomFromGeoJSON(:origin), 0.14)': {
          order: 'ASC',
          nulls: 'NULLS LAST'
        }
      })
      .setParameters({
        origin: JSON.stringify({
          type: 'Point',
          coordinates: [lat, lng]
        }),
        isLike: false
      });
    const shops = await query.getMany();
    res.send(shops);
  };

  static listPreferred = async (_: Request, res: Response) => {
    const {
      coordinates: { lat, lng }
    }: JWTPayloadInterface = res.locals.jwtPayload;
    const shopRepository = getRepository(Shop);
    const shops = await shopRepository
      .createQueryBuilder('shop')
      .leftJoinAndSelect('shop.userShops', 'userShops')
      .where('userShops.isLike = :isLike') // 14 km
      .orderBy({
        'ST_DWithin(shop.coordinates, ST_GeomFromGeoJSON(:origin), 1000000)': {
          order: 'ASC',
          nulls: 'NULLS LAST'
        }
      })
      .setParameters({
        origin: JSON.stringify({
          type: 'Point',
          coordinates: [lat, lng]
        }),
        isLike: true
      })
      .getMany();
    res.send(shops);
  };

  static newShop = async (req: Request, res: Response) => {
    const shopInputs: ShopInputInterface = req.body;
    const {
      name,
      coordinates: { lat, lng }
    } = shopInputs;
    const shop = new Shop();
    shop.name = name;
    shop.coordinates = {
      type: 'Point',
      coordinates: [lat, lng]
    };
    const shopRepository = getRepository(Shop);
    try {
      await shopRepository.save(shop);
    } catch (e) {
      res.status(409).send('shop already exists');
      return;
    }
    res.send(shop);
  };

  static likeOrDisLikeOrRemove = async (
    req: Request,
    res: Response,
    isLike: boolean = true,
    isRemove: boolean = false
  ) => {
    try {
      const { id }: JWTPayloadInterface = res.locals.jwtPayload;
      const { shopID }: ShopLikeInputInterface = req.body;
      const user = await UserController.getOneById(id);
      const shop = await ShopController.getOneById(shopID);
      if (!shop) {
        return res.status(500).send('No Shop found');
      }
      const userShopRepository = getRepository(UserShop);
      let userShop: UserShop | undefined;
      userShop = await userShopRepository.findOne({
        where: {
          user,
          shop
        }
      });
      if (!userShop) {
        userShop = new UserShop();
        userShop.user = user!;
        userShop.shop = shop;
      }
      if (isRemove) {
        userShop.isRemoved = true;
        userShop.lastRemoveDate = new Date();
      } else {
        userShop.isLike = isLike;
      }
      userShopRepository.save(userShop);
      res.status(200).send();
      return;
    } catch (e) {
      return res.status(409).json(e);
    }
  };

  static like = async (req: Request, res: Response) => {
    ShopController.likeOrDisLikeOrRemove(req, res);
  };

  static disLike = async (req: Request, res: Response) => {
    ShopController.likeOrDisLikeOrRemove(req, res, false, true);
  };

  static remove = async (req: Request, res: Response) => {
    ShopController.likeOrDisLikeOrRemove(req, res, false);
  };
}

export default ShopController;

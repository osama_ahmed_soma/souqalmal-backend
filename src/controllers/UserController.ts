import { validate } from 'class-validator';
import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import UserInputInterface from '../interfaces/UserInputInterface';
import { checkJwt } from '../middlewares/checkJWT';
import { getTokenList } from '../utils';
import AuthController from './AuthController';

class UserController {
  static getOneById = async (id: number) => {
    const userRepository = getRepository(User);
    try {
      return userRepository.findOneOrFail(id, {
        select: ['id', 'email', 'createdAt', 'updatedAt']
      });
    } catch (error) {
      return undefined;
    }
  };

  static newUser = async (req: Request, res: Response) => {
    //Get parameters from the body
    let { name, email, password, coordinates }: UserInputInterface = req.body;
    let user = new User();
    user.name = name;
    user.email = email;
    user.password = password;

    //Validade if the parameters are ok
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Hash the password, to securely store on DB
    user.hashPassword();

    //Try to save. If fails, the username is already in use
    const userRepository = getRepository(User);
    try {
      await userRepository.save(user);
    } catch (e) {
      res.status(409).send('email already in use');
      return;
    }

    const {
      status,
      code,
      token,
      refreshToken
    } = await AuthController.checkLoginAndGenrateToken({
      email,
      password,
      coordinates
    });
    if (!status) {
      res.status(code).send();
      return;
    }
    res.send({ token, refreshToken });
  };

  static login = async (req: Request, res: Response) => {
    await AuthController.login(req, res);
  };

  static async refresh(req: Request, res: Response) {
    const { refreshToken } = req.body;
    const tokenList = await getTokenList();
    if (refreshToken && refreshToken in tokenList) {
      checkJwt(req, res, () => res.status(200), refreshToken, true);
    } else {
      res.sendStatus(401);
    }
  }
}

export default UserController;

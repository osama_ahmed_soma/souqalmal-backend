import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import config from '../config/config';
import { User } from '../entity/User';
import JWTPayloadInterface from '../interfaces/JWTPayloadInterface';
import LoginMethodArgsInterface from '../interfaces/LoginMethodArgsInterface';
import LoginMethodReturnInterface from '../interfaces/LoginMethodReturnInterface';
import LoginMethodReturnOptionalInterface from '../interfaces/LoginMethodReturnOptionalInterface';

class AuthController {
  static checkLoginAndGenrateToken = async ({
    email,
    password,
    coordinates
  }: LoginMethodArgsInterface): Promise<
    LoginMethodReturnOptionalInterface | LoginMethodReturnInterface
  > => {
    //Check if username and password are set
    if (!(email && password)) {
      return {
        status: false,
        code: 400
      };
    }

    //Get user from database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail({ where: { email } });
    } catch (error) {
      return {
        status: false,
        code: 401
      };
    }

    //Check if encrypted password match
    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      return {
        status: false,
        code: 401
      };
    }

    //Sing JWT, valid for 1 hour
    const jwtPayload: JWTPayloadInterface = {
      id: user.id,
      name: user.name,
      email: user.email,
      coordinates
    };
    const token = jwt.sign(jwtPayload, config.jwt.secret, {
      expiresIn: config.jwt.expires
    });
    const refreshToken = jwt.sign(jwtPayload, config.refreshJWT.secret, {
      expiresIn: config.refreshJWT.expires
    });

    return {
      status: true,
      code: 200,
      token,
      refreshToken
    };
  };

  static login = async (req: Request, res: Response) => {
    const {
      status,
      code,
      token,
      refreshToken
    } = await AuthController.checkLoginAndGenrateToken(req.body);
    if (!status) {
      res.status(code).send();
      return;
    }
    res.send({ token, refreshToken });
  };
}
export default AuthController;

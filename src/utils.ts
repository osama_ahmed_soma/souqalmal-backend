import CoordinatesInterface from './interfaces/CoordinatesInterface';
import { redis } from './redis';
import { TOKEN_LIST } from './types';

export const arePointsNear = (
  checkPoint: CoordinatesInterface,
  centerPoint: CoordinatesInterface,
  km: number
): boolean => {
  const ky = 40000 / 360;
  const kx = Math.cos((Math.PI * centerPoint.lat) / 180.0) * ky;
  const dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
  const dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
  return Math.sqrt(dx * dx + dy * dy) <= km;
};

export const getTokenList = async () => {
  let tokenList = {};

  try {
    const tList = await redis.get(TOKEN_LIST);
    if (tList) {
      tokenList = tList;
    }
  } catch (e) {}

  return tokenList;
};

export const setTokenList = async (tokenData: undefined | null | any) => {
  if (tokenData) {
    const data = { ...(await getTokenList()), ...tokenData };
    redis.set(TOKEN_LIST, data);
  }
};

import CoordinatesInterface from './CoordinatesInterface';

export default interface JWTPayloadInterface {
  id: number;
  name: string;
  email: string;
  coordinates: CoordinatesInterface;
}

import CoordinatesInterface from './CoordinatesInterface';

export default interface ShopInputInterface {
  name: string;
  coordinates: CoordinatesInterface;
}

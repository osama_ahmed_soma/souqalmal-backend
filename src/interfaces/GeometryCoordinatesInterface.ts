export default interface GeometryCoordinatesInterface {
  type: string;
  coordinates: number[];
}

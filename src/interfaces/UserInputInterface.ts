import CoordinatesInterface from './CoordinatesInterface';

export default interface UserInputInterface {
  name: string;
  email: string;
  password: string;
  coordinates: CoordinatesInterface;
}

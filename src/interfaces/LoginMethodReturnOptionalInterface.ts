import LoginMethodReturnInterfaceBase from './LoginMethodReturnInterfaceBase';

export default interface LoginMethodReturnOptionalInterface
  extends LoginMethodReturnInterfaceBase {
  token?: undefined;
  refreshToken?: undefined;
}

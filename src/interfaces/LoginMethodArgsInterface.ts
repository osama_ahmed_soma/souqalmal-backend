import CoordinatesInterface from './CoordinatesInterface';

export default interface LoginMethodArgsInterface {
  email: string;
  password: string;
  coordinates: CoordinatesInterface;
}

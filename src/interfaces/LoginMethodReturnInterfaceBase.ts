export default interface LoginMethodReturnInterfaceBase {
  status: boolean;
  code: number;
}

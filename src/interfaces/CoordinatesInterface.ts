export default interface CoordinatesInterface {
  lat: number;
  lng: number;
}

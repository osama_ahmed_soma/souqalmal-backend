import LoginMethodReturnInterfaceBase from './LoginMethodReturnInterfaceBase';

export default interface LoginMethodReturnInterface
  extends LoginMethodReturnInterfaceBase {
  token: string;
  refreshToken: string;
}

import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config/config';
import JWTPayloadInterface from '../interfaces/JWTPayloadInterface';

export const checkJwt = (
  req: Request,
  res: Response,
  next: NextFunction,
  token: string | undefined = undefined,
  isSecret: boolean = false
) => {
  token = token ? token : <string>req.headers['authorization'];

  try {
    const jwtPayload = <JWTPayloadInterface>(
      jwt.verify(token, isSecret ? config.refreshJWT.secret : config.jwt.secret)
    );
    res.locals.jwtPayload = jwtPayload;
    const { id, email, name, coordinates } = jwtPayload;
    const newToken = jwt.sign(
      { id, email, name, coordinates },
      config.jwt.secret,
      {
        expiresIn: config.jwt.expires
      }
    );
    res.setHeader('token', newToken);

    next();
  } catch (error) {
    res.status(401).send();
    return;
  }
};

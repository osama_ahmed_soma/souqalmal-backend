import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  ObjectID,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Shop } from './Shop';
import { User } from './User';

@Entity()
export class UserShop extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @ManyToOne(
    () => User,
    user => user.userShops
  )
  user: User;

  @ManyToOne(
    () => Shop,
    shop => shop.userShops
  )
  shop: Shop;

  @Column({ nullable: true })
  isLike?: boolean;

  @Column({ nullable: true })
  isRemoved?: boolean;

  @Column({ nullable: true })
  lastRemoveDate?: Date;
}

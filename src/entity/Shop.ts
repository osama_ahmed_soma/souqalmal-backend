import {
  BaseEntity,
  Column,
  Entity,
  Index,
  ObjectID,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import GeometryCoordinatesInterface from '../interfaces/GeometryCoordinatesInterface';
import { UserShop } from './UserShop';

@Entity()
export class Shop extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column()
  @Index({ unique: true })
  name: string;

  @Column({ nullable: true })
  picture?: string;

  @Column('geometry', {
    spatialFeatureType: 'Point'
  })
  coordinates: GeometryCoordinatesInterface;

  @OneToMany(
    () => UserShop,
    userShop => userShop.shop
  )
  public userShops: UserShop[];
}

#!/bin/bash

yarn build
docker build -t osama/souqalmal:latest . --no-cache
heroku container:push --app=souqalmal web
heroku container:release --app=souqalmal web